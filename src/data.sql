


CREATE TABLE `users` (
`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`firstname` CHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ,
`lastname` CHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ,
`login` CHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ,
`password` CHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_bin;

INSERT INTO `users` (`id` , `firstname` , `lastname` , `login` , `password`)
VALUES (NULL ,  'John',  'Doe',  'john.doe', ENCRYPT(  'John0Doe1' ));
