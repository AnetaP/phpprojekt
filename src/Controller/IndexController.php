<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller; 

use Silex\Application;   
use Silex\ControllerProviderInterface;   
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\Validator\Constraints as Assert;

/**
* Define index methods.
*/
class indexController implements ControllerProviderInterface
{
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect(Application $app) 
    {
        $indexController = $app['controllers_factory'];
        $indexController->get('/', array($this, 'index'));
        return $indexController;
    }



     /**
     * Renders welcome page
     *
     * @access public
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index(Application $app)
    {    
        return $app['twig']->render('news/news.twig');
    }

}
