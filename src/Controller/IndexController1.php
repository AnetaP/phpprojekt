<?php

namespace Controller; //informacja ze bedzie to kontroler

use Silex\Application;   //zalaczenie dodatkowych bibliotek
use Silex\ControllerProviderInterface;   //
use Symfony\Component\HttpFoundation\Request; //
use Symfony\Component\Validator\Constraints as Assert;

class IndexController1 implements ControllerProviderInterface
{
    public function connect(Application $app) //koniecznie! sluzy do okreslenia co ma zostac wykonane np add
    {
        $indexController1 = $app['controllers_factory'];
        $indexController1->get('/', array($this, 'albums'));
        $indexController1->match('/add', array($this, 'add'));
        $indexController1->get('/edit', array($this, 'edit'));
        $indexController1->get('/delete', array($this, 'delete'));
        $indexController1->get('/view', array($this, 'view'));
        return $indexController1;
    }

    public function index(Application $app)
    {
        return 'Index hiszpanski ';
    }

    public function add(Application $app, Request $request)
    {

        // default values:
        $data = array(
            'title' => 'Title',
            'artist' => 'Artist',
        );


    $form = $app['form.factory']->createBuilder('form', $data)
            ->add(
                'title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                )
            )
            ->add(
                'artist', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                )
            )
            ->getForm();



        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            var_dump($data);
        }

        return $app['twig']->render('albums/add.twig', array('form' => $form->createView()));
    }


    public function edit(Application $app)
    {
        return 'Edit Action';
    }

    public function delete(Application $app)
    {
        return 'Delete Action';
    }

    public function view(Application $app)
    {
        return 'View Action';
    }

}
