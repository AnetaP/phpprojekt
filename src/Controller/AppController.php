<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Model\AppModel;
 * @uses Model\PostsModel;
 * @uses Controller\usersController;
 * @uses Silex\Application;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller;

use Model\AppModel;
use Model\PostsModel;
use Controller\usersController;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Define application methods.
 */
class appController implements ControllerProviderInterface
{
     /**
     * @access public
     * @param Application $app
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $appController = $app['controllers_factory'];
        $appController->get('/randomWord', array($this, 'randomWord'))
            ->bind('/randomWord/');
        $appController->get('/choose', array($this, 'choose'))
            ->bind('/choose/');

        $appController->match('/guessWord', array($this, 'guessWord'))
            ->bind('/guessWord/');
         $appController->match('/guessWordPl', array($this, 'guessWordPl'))
            ->bind('/guessWordPl/');
        $appController->
        match('/guessWordCat/{idCategories}', array($this, 'guessWordCat'))
        ->bind('/guessWordCat/');
          $appController->
        match('/guessWordCatPl/{idCategories}', array($this, 'guessWordCatPl'))
        ->bind('/guessWordCatPl/');
        $appController->match('/results/{idUsers}', array($this, 'results'))
            ->bind('/results/');
        return  $appController;
    }

    /**
     * Loads random word
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function randomWord (Application $app, Request $request)
    {
        $appModel = new AppModel($app);
        $application = $appModel->getRandomID();
        return $app['twig']->
        render(
            '/application/index.twig', array(
            'spanish'=> $application['spanish'], 'polish'
            => $application['polish'], 'comment' => $application['comment'])
        );
        
    }   

    /**
     * Renders the page to choose language fot the test
     *
     * @access public
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function choose(Application $app)
    {    
        return $app['twig']->render('application/choose.twig');
    }

    /**
     * The form and check of guessed word 
     *
     * Displayes random word and then checks the guess of the user. 
     * Informs if the anser is correct or not.
     * Counts number of right and wrong answers.
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function guessWord(Application $app, Request $request)
    {

        $usersController = new usersController($app); 
        $idUser = $usersController-> getIdCurrentUser($app);
       

        if (!$_SESSION[answeredID]) {
             $_SESSION[answeredID]=array();
             array_push($_SESSION[answeredID], '0'); 

        }

        if (!$_SESSION['right']) {
            $_SESSION['right']=0;

        }

        if (!$_SESSION['wrong']) {
             $_SESSION['wrong']=0;
        }


        if ($answeredID==='0') {
             $condition = '0';
        } else {
             $condition = implode(',', $_SESSION[answeredID]);
             
        
        }


        $appModel = new AppModel($app);
        $randomWord = $appModel->getRandomWord($condition);
        $rightAnswer = $randomWord['polish'];
      

        if (!$_SESSION[rightanswers]) {
        $_SESSION[rightanswers]=array();
        }
        array_push($_SESSION[rightanswers], $rightAnswer); 
        $idhelper = count($_SESSION[rightanswers]);
        $word =  $randomWord['spanish'];
        $wordID =  $randomWord['idVocabulary'];
          
        $answer = array();
        $form = $app['form.factory']->createBuilder('form', $answer)

        ->add(
            'answer', 'text', array(
            'constraints' => 
            array(new Assert\NotBlank(), 
            new Assert\Length(array('min' => 2))), 'label' => 'Odpowiedź:'
            )
        )
        ->add('Dodaj', 'submit')
        ->add('Koniec', 'submit')
        ->getForm();

        $form->handleRequest($request);
      
        if ($form->isValid()) {
            if ($form->get('Dodaj')->isClicked()) {
               $answer= $form -> getData();
               if ($answer['answer'] === $_SESSION[rightanswers][$idhelper-2]) {
                    $_SESSION['right']= $_SESSION['right']+1;
                    array_push($_SESSION[answeredID], $wordID); 
                    $app['session']->getFlashBag()->add(
                        'message', array(
                        'type' => 'success', 'content' => 'Dobrze!')
                    );
               } else {
                $goodAnswer =  $_SESSION[rightanswers][$idhelper-2];
                $_SESSION['wrong']= $_SESSION['wrong']+1;
                    $app['session']->getFlashBag()->add(
                        'message', array(
                        'type' => 'warning', 'content'
                        => "Źle! Poprawna odpowiedź to: $goodAnswer ")
                    );
               }
                if (empty($randomWord)) {
                    $result = $appModel->addFinalResult(
                        $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                    );
                    $right= $_SESSION['right'];
                    $wrong= $_SESSION['wrong'];
                    $_SESSION['right']=0;
                    $_SESSION['wrong']=0;
                    $_SESSION[answeredID]=array();
                    $_SESSION[rightanswers]=array();
                    return $app['twig']->render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );
                } else {
                    return $app['twig']->
                    render(
                        'application/guess.twig', array(
                            'form' => $form->createView(), 'randomWord' 
                                => $randomWord)
                    );
                }
            }
            if ($form->get('Koniec')->isClicked()) {
                $result = $appModel->addFinalResult(
                    $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                );
                $right= $_SESSION['right'];
                $wrong= $_SESSION['wrong'];
                $_SESSION['right']=0;
                $_SESSION['wrong']=0;
                $_SESSION[answeredID]=array();
                $_SESSION[rightanswers]=array();
                return $app['twig']->render(
                    'application/result.twig', array(
                        'right' => $right, 'wrong' => $wrong)
                );
            }    
        }
        return $app['twig']->
        render(
            'application/guess.twig', array(
                'form' => $form->createView(),'randomWord' => $randomWord)
        );
    }


    /**
     * The form and check of guessed word 
     *
     * Displayes random word and then checks the guess of the user. 
     * Informs if the anser is correct or not.
     * Counts number of right and wrong answers.
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function guessWordPl(Application $app, Request $request)
    {
        $usersController = new usersController($app); 
        $idUser = $usersController-> getIdCurrentUser($app);
        if (!$_SESSION[answeredID]) {
             $_SESSION[answeredID]=array();
             array_push($_SESSION[answeredID], '0'); 
        }
        if (!$_SESSION['right']) {
            $_SESSION['right']=0;
        }
        if (!$_SESSION['wrong']) {
             $_SESSION['wrong']=0;
        }
        if ($answeredID==='0') {
             $condition = '0';
        } else {
             $condition = implode(',', $_SESSION[answeredID]);
        }
        $appModel = new AppModel($app);
        $randomWord = $appModel->getRandomWord($condition);
        $rightAnswer = $randomWord['spanish'];

        if (!$_SESSION[rightanswers]) {
        $_SESSION[rightanswers]=array();
        }
        array_push($_SESSION[rightanswers], $rightAnswer); 
        $idhelper = count($_SESSION[rightanswers]);
        $word =  $randomWord['polish'];
        $wordID =  $randomWord['idVocabulary'];
          
        $answer = array();
        $form = $app['form.factory']->createBuilder('form', $answer)

        ->add(
            'answer', 'text', array(
            'constraints' => 
            array(new Assert\NotBlank(), 
            new Assert\Length(array('min' => 2))), 'label' => 'Odpowiedź:'
            )
        )
        ->add('Dodaj', 'submit')
        ->add('Koniec', 'submit')
        ->getForm();

        $form->handleRequest($request);
      
        if ($form->isValid()) {
            if ($form->get('Dodaj')->isClicked()) {
                $answer= $form -> getData();
                if (
                   $answer['answer'] === $_SESSION[rightanswers][$idhelper-2]) {
                   $_SESSION['right']= $_SESSION['right']+1;
                   array_push($_SESSION[answeredID], $wordID); 
                   $app['session']->getFlashBag()->add(
                       'message', array(
                        'type' => 'success', 'content' => 'Dobrze!')
                   );
                } else {
                    $goodAnswer =  $_SESSION[rightanswers][$idhelper-2];
                    $_SESSION['wrong']= $_SESSION['wrong']+1;
                    $app['session']->getFlashBag()->add(
                        'message', array('type' => 'warning', 'content' 
                            => "Źle! Poprawna odpowiedź to: $goodAnswer ")
                    );
                }
            if (empty($randomWord)) {
                $result = $appModel->
                addFinalResult(
                    $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                );
                $right= $_SESSION['right'];
                $wrong= $_SESSION['wrong'];
                $_SESSION['right']=0;
                $_SESSION['wrong']=0;
                $_SESSION[answeredID]=array();
                $_SESSION[rightanswers]=array();
                return $app['twig']->render(
                    'application/result.twig', array(
                    'right' => $right, 'wrong' => $wrong)
                );
            } else {
                return $app['twig']->
                render(
                    'application/guesspl.twig', array(
                    'form' => $form->createView(), 'randomWord' => $randomWord)
                );
            }
            }
                if ($form->get('Koniec')->isClicked()) {
                    $result = $appModel->addFinalResult(
                        $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                    );                    
                    $right= $_SESSION['right'];
                    $wrong= $_SESSION['wrong'];
                    $_SESSION['right']=0;
                    $_SESSION['wrong']=0;
                    $_SESSION[answeredID]=array();
                    $_SESSION[rightanswers]=array();
                    return $app['twig']->render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );
                }    
        }
        return $app['twig']->
        render(
            'application/guesspl.twig', array(
            'form' => $form->createView(),'randomWord' => $randomWord)
        );
    }


     /**
     * The form and check of guessed word from category
     *
     * Displayes random word and then checks the guess of the user. 
     * Informs if the anser is correct or not.
     * Counts number of right and wrong answers.
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function guessWordCat(Application $app, Request $request)
    {     
        $usersController = new usersController($app); 
        $idUser = $usersController-> getIdCurrentUser($app);
        $idCategories = (int) $request->get('idCategories', 0);
        if (!$_SESSION[answeredID]) {
             $_SESSION[answeredID]=array();
             array_push($_SESSION[answeredID], '0'); 
        }
        if (!$_SESSION['right']) {
            $_SESSION['right']=0;
        }
        if (!$_SESSION['wrong']) {
             $_SESSION['wrong']=0;
        }
        if ($answeredID==='0') {
             $condition = '0';
        } else {
             $condition = implode(',', $_SESSION[answeredID]);
        }  
        $appModel = new AppModel($app);
        $randomWord = $appModel->getRandomWordCat($condition, $idCategories);
        $rightAnswer = $randomWord['polish'];
        if (!$_SESSION[rightanswers]) {
            $_SESSION[rightanswers]=array();
        }
        array_push($_SESSION[rightanswers], $rightAnswer); 
        $idhelper = count($_SESSION[rightanswers]);
        $word =  $randomWord['spanish'];
        $wordID =  $randomWord['idVocabulary'];
        $answer = array();
            $form = $app['form.factory']->createBuilder('form', $answer)
            ->add(
                'answer', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 2)
                    )
                    ),'label' => 'Odpowiedź:'
                )
            )
            ->add('Dodaj', 'submit')
            ->add('Koniec', 'submit')
            ->getForm();
            $form->handleRequest($request);
      
        if ($form->isValid()) {
            if ($form->get('Dodaj')->isClicked()) {
               $answer= $form -> getData();
               if ($answer['answer'] === $_SESSION[rightanswers][$idhelper-2]) {
                $_SESSION['right']= $_SESSION['right']+1;
                array_push($_SESSION[answeredID], $wordID); 
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'success', 'content' => 'Dobrze!')
                );
               } else {
                    $_SESSION['wrong']= $_SESSION['wrong']+1;
                    $goodAnswer =  $_SESSION[rightanswers][$idhelper-2];
                    $app['session']->getFlashBag()->add(
                        'message', array(
                            'type' => 'warning', 'content' 
                            => "Źle! Poprawna odpowiedź to: $goodAnswer ")
                    );
               }

                if (empty($randomWord)) {
                    $result = $appModel->addFinalResult(
                        $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                    );
                    $right= $_SESSION['right'];
                    $wrong= $_SESSION['wrong'];
                    $_SESSION['right']=0;
                    $_SESSION['wrong']=0;
                    $_SESSION[answeredID]=array();
                    $_SESSION[rightanswers]=array();

                    return $app['twig']->render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );

                } else {

                return $app['twig']->
                render(
                    'application/guess.twig', array(
                    'form' => $form->createView(), 'randomWord' => $randomWord)
                );
                }
            }
            if ($form->get('Koniec')->isClicked()) {
                $result = $appModel->addFinalResult(
                    $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                );
                $right= $_SESSION['right'];
                $wrong= $_SESSION['wrong'];
                $_SESSION['right']=0;
                $_SESSION['wrong']=0;
                $_SESSION[answeredID]=array();
                $_SESSION[rightanswers]=array();

                    return $app['twig']->render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );

            }    
        }
        return $app['twig']->
        render(
            'application/guess.twig', array(
                'form' => $form->createView(),'randomWord' => $randomWord)
        );
    }


    /**
     * The form and check of guessed word from category
     *
     * Displayes random word and then checks the guess of the user. 
     * Informs if the anser is correct or not.
     * Counts number of right and wrong answers.
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function guessWordCatPl(Application $app, Request $request)
    {     

        $usersController = new usersController($app); 
        $idUser = $usersController-> getIdCurrentUser($app);
        $idCategories = (int) $request->get('idCategories', 0);

        if (!$_SESSION[answeredID]) {
             $_SESSION[answeredID]=array();
             array_push($_SESSION[answeredID], '0'); 
        }
        if (!$_SESSION['right']) {
            $_SESSION['right']=0;
        }
        if (!$_SESSION['wrong']) {
             $_SESSION['wrong']=0;
        }
        if ($answeredID==='0') {
             $condition = '0';
        } else {
             $condition = implode(',', $_SESSION[answeredID]);
        }
        $appModel = new AppModel($app);
        $randomWord = $appModel->getRandomWordCat($condition, $idCategories);
        $rightAnswer = $randomWord['spanish'];
        if (!$_SESSION[rightanswers]) {
        $_SESSION[rightanswers]=array();
        }
        array_push($_SESSION[rightanswers], $rightAnswer); 
        $idhelper = count($_SESSION[rightanswers]);
        $word =  $randomWord['polish'];
        $wordID =  $randomWord['idVocabulary'];
        $answer = array();
        $form = $app['form.factory']->createBuilder('form', $answer)

        ->add(
            'answer', 'text', array(
            'constraints' => array(
                new Assert\NotBlank(), new Assert\Length(
                    array('min' => 2)
                )
                ), 'label' => 'Odpowiedź:'
            )
        )
        ->add('Dodaj', 'submit')
        ->add('Koniec', 'submit')
        ->getForm();

        $form->handleRequest($request);
      
        if ($form->isValid()) {
            if ($form->get('Dodaj')->isClicked()) {
               $answer= $form -> getData();
               if ($answer['answer'] === $_SESSION[rightanswers][$idhelper-2]) {
               $_SESSION['right']= $_SESSION['right']+1;
               array_push($_SESSION[answeredID], $wordID); 
               $app['session']->getFlashBag()->add(
                   'message', array('type' => 'success', 'content' => 'Dobrze!')
               );
               } else {
                    $_SESSION['wrong']= $_SESSION['wrong']+1;
                    $goodAnswer =  $_SESSION[rightanswers][$idhelper-2];
                    $app['session']->getFlashBag()->add(
                        'message', array('type' => 'warning', 'content' 
                            => "Źle! Poprawna odpowiedź to: $goodAnswer ")
                    );
               }
               if (empty($randomWord)) {
                    $result = $appModel->addFinalResult(
                        $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                    );
                    $right= $_SESSION['right'];
                    $wrong= $_SESSION['wrong'];
                    $_SESSION['right']=0;
                    $_SESSION['wrong']=0;
                    $_SESSION[answeredID]=array();
                    $_SESSION[rightanswers]=array();
                    return $app['twig']->
                    render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );
               } else {
                return $app['twig']->
                render(
                    'application/guesspl.twig', array(
                        'form' => $form->createView(), 'randomWord' 
                            => $randomWord)
                );
               }
            }
            if ($form->get('Koniec')->isClicked()) {
                $result = $appModel->addFinalResult(
                    $_SESSION['right'], ($_SESSION['wrong']), ($idUser)
                );
                $right= $_SESSION['right'];
                $wrong= $_SESSION['wrong'];
                $_SESSION['right']=0;
                $_SESSION['wrong']=0;
                $_SESSION[answeredID]=array();
                $_SESSION[rightanswers]=array();

                    return $app['twig']->render(
                        'application/result.twig', array(
                            'right' => $right, 'wrong' => $wrong)
                    );

            }    
        }
        return $app['twig']->
        render(
            'application/guesspl.twig', array(
                'form' => $form->createView(),'randomWord' => $randomWord)
        );
    }

    /**
     * Test's results
     *
     * Displays results from all test taken by the user
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

 public function results (Application $app, Request $request)
 {

    $appModel = new AppModel($app);  
    $usersController = new usersController($app); 
    $currentUser = (int) $usersController->getIdCurrentUser($app);
    $idUsers = (int) $request->get('idUsers');
      
    $results = $appModel->getResults($idUsers);

    return $app['twig']->
    render(
        '/application/results.twig', array(
            'Results'=>$results,'idUsers'=>$idUsers,'current'=>$currentUser)
    );
 }


}
