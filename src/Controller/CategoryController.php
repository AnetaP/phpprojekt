<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Model\CategoryModel;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller;
use Model\CategoryModel;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


/**
* Define categories methods.
*/
class categoryController implements ControllerProviderInterface
{
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect(Application $app)
    {
        $categoryController = $app['controllers_factory'];
        $categoryController->get('/', array($this, 'index'))
        ->bind('/categories/');
        $categoryController->match('/public', array($this, 'publicCategories'))
        ->bind('/categories/public/');
        $categoryController->match('/add', array($this, 'add'))
        ->bind('/categories/add/');
        $categoryController->match('/edit/{idCategories}', array($this, 'edit'))
        ->bind('/categories/edit/');
        $categoryController
        ->match('/userCategories/{idUsers}', array($this, 'userCategories'))
        ->bind('/categories/userCategories/');
        $categoryController
        ->match('/delete/{idCategories}', array($this, 'delete'))
        ->bind('/categories/delete/');
        $categoryController->get('/view', array($this, 'view'))
        ->bind('/categories/view/');
        return  $categoryController;
    }
 
    /**
     * Categories list
     *
     * Displays the list of all public categories
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array Categories
     */
    public function index (Application $app, Request $request)
    {


        $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getPublic();
        return $app['twig']->render(
            '/categories/all.twig', array('Categories' => $categories)
        );
    
    }


    /**
     * User's private categories list
     *
     * Displays the list of categories of the user
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array Categories
     */
    public function userCategories (Application $app, Request $request)
    {
        $usersController = new usersController($app); 
        $idUsers = (int) $request->get('idUsers');
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);

       
        $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getUserCategories($idUsers);
        return $app['twig']->render(
            '/categories/index.twig', array(
                'Categories' => $categories, 'idUsers' 
                => $idUsers, 'current' => $currentUser)
        );
    }

    /**
     * Public list
     *
     * Displays the list of all public categories
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array Categories
     */
    public function publicCategories (Application $app, Request $request)
    {
       $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getPublic();
        return $app['twig']->render(
            '/categories/index.twig', array(
                'Categories' => $categories)
        );
    }

     /**
     * Adds a new category
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return mixed
     */
    public function add(Application $app, Request $request)
    {

         $usersController = new usersController($app); 
         $idUsers = $usersController-> getIdCurrentUser($app);
         $typeCategories = 0;

        
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)

            ->add(
                'nameCategories', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 2)
                    )
                )
                )
            )
            ->add('Dodaj', 'submit')
            ->getForm();

        $form->handleRequest($request);


        if ($form->isValid()) {
                $data = $form->getData();

             $categoryModel = new categoryModel($app);
             $category = $categoryModel
             ->getCategoryByName($data['nameCategories'], $idUsers);
             if ($category== null) {

             $categoryModel->addCategory(
                 $form->getData(), $typeCategories, $idUsers
             );

             $app['session']->getFlashBag()->add(
                 'message', array(
                 'type' => 'success', 'content'
                  => 'Nowa kategoria została dodana!')
             );
             return $app->redirect('./userCategories/'.$idUsers, 301);

             }
    
        $app['session']->getFlashBag()->add(
            'message', array(
                'type' => 'warning', 'content' 
                => 'Już masz kategorię o takiej nazwie!')
        );
        return $app['twig']->render(
            'categories/add.twig', array(
                'form' => $form->createView(), 'idUsers' => $idUsers)
        );
        }
        return $app['twig']->render(
            'categories/add.twig', array(
            'form' => $form->createView(), 'idUsers' => $idUsers)
        );
    }

   /**
     * Edit category
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return mixed
     */
    public function edit(Application $app, Request $request)
    {
        $categoryModel = new categoryModel($app);
        $idCategories = (int) $request->get('idCategories', 0);
        $category = $categoryModel->getCategory($idCategories);
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
        $catUser= (int) $category['idUser'];
        if (count($category)) {

            $form = $app['form.factory']->createBuilder('form', $category)
                ->add(
                    'idCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'nameCategories', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 2)
                        )),'label' => 'Nazwa kategorii'
                    )
                )
                ->add('Zapisz', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $categoryModel = new CategoryModel($app);
                $categoryModel->saveCategory($form->getData());
                return $app->redirect('../userCategories/'.$catUser, 301);
            }
            return $app['twig']->render(
                'categories/edit.twig', array(
                    'form' => $form->createView(), 'category' 
                    => $category, 'catUser' 
                    => $catUser, 'current' => $currentUser)
            );
        } else {
           return $app->redirect(
               $app['url_generator']->generate('/categories/add/'), 301
           );
        }
    }

    /**
     * Delete category and all vocabulary from this category
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
     public function delete(Application $app, Request $request)
     {
        $categoryModel = new categoryModel($app);
        $idCategories = (int) $request->get('idCategories', 0);
        $category = $categoryModel->getCategory($idCategories);
        $idCategories = (int) $request->get('idCategories');
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
        $catUser= (int) $category['idUser'];

        if (
            ctype_digit((string)$idCategories)
                 && $categoryModel->idExist($idCategories)
            ) {
            $category = $categoryModel->getCategory($idCategories);
        } else {
            return $app->redirect(
                $app['url_generator']->generate('/categories/'), 301
            );
        }

        if (count($category)) {

            $form = $app['form.factory']->createBuilder('form', $category)
                ->add(
                    'idCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'nameCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank(
                    ), new Assert\Length(array('min' => 2))
                    )
                    )
                )
                ->add(
                    'typeCategories', 'hidden', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 1)
                        )
                    )
                    )
                )
                ->add('Usuń', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $categoryModel = new categoryModel($app);
                $categoryModel->deleteCategory($form->getData());
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'success', 'content' => 'Usunięto kategorię!')
                );
                return $app->redirect('../userCategories/'.$catUser, 301);
            }

            return $app['twig']->render(
                'categories/delete.twig', array(
                    'form' => $form->createView(), 'Category' 
                    => $category, 'catUser' => $catUser, 'current' 
                    => $currentUser)
            );

        } else {
            return $app->redirect(
                $app['url_generator']->generate('/categories/'), 301
            );
        }
     }

}

