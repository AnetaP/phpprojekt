<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Controller
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Model\UsersModel;
 * @uses Silex\ControllerProviderInterface;
 * @uses Symfony\Component\HttpFoundation\Request;
 * @uses Symfony\Component\Validator\Constraints as Assert;
 */
namespace Controller;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\UsersModel;

/**
* Define users methods.
*/
class usersController implements ControllerProviderInterface
{
    /**
    * @access public
    * @param Application $app
    * @return \Silex\ControllerCollection
    */
    public function connect(Application $app)
    {
        $usersController = $app['controllers_factory'];
        $usersController->get('/', array($this, 'index'));
        $usersController->match('/add', array($this, 'add'))->bind('/add/');
        $usersController->match('/edit/{idUser}', array($this, 'edit'))
        ->bind('/users/edit/');
        $usersController->match('/delete/{idUser}', array($this, 'delete'))
        ->bind('/users/delete/');
        $usersController->get('/view', array($this, 'currentUser'))
        ->bind('/users/view/');
        return  $usersController;
    }

    /**
     * Gets the list of the users
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array users
     */
    public function index (Application $app, Request $request)
    {
        $usersModel = new usersModel($app);
        $users = $usersModel->getAll();
        return $app['twig']->render(
            '/users/index.twig', array('users' => $users)
        );
    }


    /**
     * Gets current User
     *
     * @access private
     * @param Application $app
     * @return $user
     */
    protected function getCurrentUser($app)
    {
    $token = $app['security']->getToken();

    if (null !== $token) {
        $user = $token->getUser()->getUsername();
    }

    return $user;
    }

    /**
     * Gets current user's id
     *
     * @access public
     * @param Application $app
     * @return $iduser
     */
    public function getIdCurrentUser($app)
    {

    $login = $this->getCurrentUser($app);
    $usersModel = new usersModel($app);
    $iduser = $usersModel->getUserByLogin($login);
    return $iduser['id'];
    }


     /**
     * Gets current user's name
     *
     * @access public
     * @param Application $app
     * @return $iduser
     */
    public function getNameCurrentUser($app)
    {

    $login = $this->getCurrentUser($app);
    $usersModel = new usersModel($app);
    $iduser = $usersModel->getUserByLogin($login);
    return $iduser['login'];
    }


   /**
     * Gets current User
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return array user data
     */
    public function currentUser(Application $app, Request $request)
    {
  
    $login = $this->getCurrentUser($app);
    
    $usersModel = new usersModel($app);
    $user = $usersModel->getUserByLogin($login);
    
    return $app['twig']->render(
        'users/view.twig', array(
            'idUser' => $user['id'], 'login' 
            => $user['login'], 'email' => $user['email']
        )
    );
    }

     /**
     * Register new user
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add(Application $app, Request $request)
    {
         $form = $app['form.factory']->createBuilder('form', $data)
            ->add(
                'login', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 5)
                    )
                      ), 'label' => 'Login'
                )
            )
            ->add(
                'password', 'password', array(
                'constraints' => array(
                  new Assert\NotBlank(), new Assert\Length(
                      array('min' => 6)
                  )), 'label' => 
                  'Hasło'
                )
            )
            ->add(
                'confirm_password', 'password', array(
                'constraints' => array(
                  new Assert\NotBlank(), new Assert\Length(
                      array('min' => 6)
                  )), 'label' => 
                  'Potwierdź hasło'
                )
            )
            ->add(
                'email', 'email', array(
                'constraints' => array(
                  new Assert\NotBlank(), new Assert\Length(
                      array('min' => 5)
                  )), 'label' => 
                  'Email' 
                )
            )
           ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
       
            if ($data['password'] === $data['confirm_password']) {
                $data['password'] = $app['security.encoder.digest']
                    ->encodePassword("{$data['password']}", '');
                $userModel = new usersModel($app);

                $user = $userModel->getUserByLogin($data['login']);     
                
                if ($user == null) {

                $userModel->Register($data);
                $id= $app['db']->lastInsertId(); 
        
                 $userModelA = new usersModel($app);
                 $userModelA->AddUserRole($id);

                if ($userModel) {
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'success', 'content'
                       => 'Rejestracja zakończyła się sukcesem. Zaloguj się'
                    )
                );
                return $app->redirect(
                    $app['url_generator']->generate("/auth/login"), 301
                );
                }
                }
                $app['session']->getFlashBag()->add(
                    'message', array('type' => 'warning', 'content' 
                        => 'Ten login juz istnieje')
                );
                return $app['twig']->render(
                    'users/add.twig', array('form' 
                    => $form->createView()
                    )
                );
           
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array('type' => 'warning', 'content' 
                        => 'Hasła różnią się od siebie')
                );
                return $app['twig']->render(
                    'users/add.twig', array('form' => $form->createView())
                );
            }
        }
        return $app['twig']->render(
            'users/add.twig', array('form' => $form->createView())
        );
    }


    /**
     * Edit user
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Application $app, Request $request)
    {
        $pass = 0;
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);

        $idUsers = (int) $request->get('idUser');

        $usersModel = new usersModel($app);
        $idUser = (int) $request->get('idUser', 0);
 
        $user = $usersModel->getUserbyID($idUser);

        if (count($user)) {

            $form = $app['form.factory']->createBuilder('form', $user)
                ->add(
                    'id', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )

                ->add(
                    'email', 'email', array(
                    'constraints' => array(
                    new Assert\NotBlank(), new Assert\Length(
                        array('min' => 5)
                    )), 'label' => 
                    'E-mail' 
                    )
                )  

                 ->add(
                     'new_password', 'password', array(
                     'constraints' => array(new Assert\NotBlank()), 'label' 
                        => 'Nowe hasło'
                     )
                 )

                ->add(
                    'confirm_password', 'password', array(
                    'constraints' => array(new Assert\NotBlank()), 'label' 
                        => 'Potwierdź hasło'
                    )
                )

                 ->add(
                     'password', 'password', array(
                     'constraints' => array(new Assert\NotBlank()), 'label' 
                        => 'Stare hasło'
                     )
                 )

               ->add('Zapisz', 'submit')
               ->getForm();
            $form->handleRequest($request);
           
            if ($form->isValid()) {
                $data= $form->getData();
                $data['password'] = $app['security.encoder.digest']
                ->encodePassword("{$data['password']}", '');
           
            if ($data['password']=== $user['password']) {
                if ($data['new_password'] === $data['confirm_password']) {
                $data['new_password'] = $app['security.encoder.digest']
                ->encodePassword("{$data['new_password']}", '');
          
                $usersModel = new usersModel($app);
                $usersModel->saveUser($data);
            
                return $app->redirect(
                    $app['url_generator']->generate('/users/view/'), 301
                );
                }
            }  
        $pass = 1;
        return $app['twig']->render(
            'users/edit.twig', array(
                'form' => $form->createView(), 'user' => $user, 'idUsers' 
                => $idUsers, 'current' => $currentUser, 'pass' => $pass)
        );

            }
            return $app['twig']->render(
                'users/edit.twig', array(
                    'form' => $form->createView(), 'user' => $user, 'idUsers' 
                    => $idUsers, 'current' => $currentUser, 'pass' => $pass)
            );

        } else {

           return $app->redirect(
               $app['url_generator']->generate('/users/add/'), 301
           );
        }

    }
    
   /**
     * Delete user
     *
     * @access public
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
  public function delete(Application $app, Request $request)
  {
        
        $usersController = new usersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);

        $usersModel = new usersModel($app);
        $idUser = (int) $request->get('idUser', 0);
     
        $user = $usersModel->getUserbyID($idUser);

        if (count($user)) {

            $form = $app['form.factory']->createBuilder('form', $user)
                ->add(
                    'id', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                      ->add(
                          'password', 'hidden', array(
                          'constraints' => array(new Assert\NotBlank(), 
                            new Assert\Length(array('min' => 5)))
                          )
                      )
             
                ->add('Usuń', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                if ($data['id'] != 1) {
                $usersModel = new usersModel($app);
                $usersModel->deleteUser($form->getData());
                $app['session']->
                getFlashBag()->add(
                    'message', array('type' => 'success', 'content'
                     => 'Usunięto użytkownika!')
                );
            return $app->redirect(
                $app['url_generator']->generate('users'), 301
            );
                }  
            $app['session']->getFlashBag()->add(
                'message', array('type' => 'warning', 'content' 
                => 'Nie możesz usunąc konta administratora!')
            );   
            return $app->redirect(
                $app['url_generator']->generate('users'), 301
            );
 
            }
            return $app['twig']->
            render(
                'users/delete.twig', 
                array('form' => $form->createView(), 'user' => $user,
                'idUser' => $idUser, 'current' => $currentUser)
            );

         
        } else {
            return $app->redirect(
                $app['url_generator']->generate('users'), 301
            );
        }
  }  
  
}

