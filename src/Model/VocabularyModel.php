<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Model
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\Provider\DoctrineServiceProvider;
 */
namespace Model;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

/**
* Define vocabulary database methods.
*/
class vocabularyModel
{
    /**
    * Database access object.
    *
    * @access protected
    * @var $_db Doctrine\DBAL
    */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app 
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

     /**
     * Add new category
     *
     * @access public
     * @param array $data
     * @param int $idCategories
     * @param int $idUsers
     * @return Void
     */
    public function addVocabulary($data, $idCategories, $idUsers)
    {
        $sql = 'INSERT INTO Vocabulary (
            polish, spanish, comment, idCategories, idUsers) VALUES (
            ?,?,?,?,?)';
        $this->_db->executeQuery(
            $sql, array( 
            $data['polish'], 
            $data['spanish'], $data['comment'], $idCategories, $idUsers)
        );
    }

    /**
     * Get all words
     *
     * @access public
     * @return array 
     */
    public function getAll()
    {
        $sql = 'SELECT idVocabulary, polish, spanish, 
        comment, type, idCategories, idUsers FROM Vocabulary';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Get words from a certain category
     *
     * @access public
     * @param int $idCategories
     * @return array 
     */
    public function getFromCategory($idCategories)
    {
        $sql = "SELECT *
        FROM Vocabulary WHERE idCategories = '$idCategories'";
        return $this->_db->fetchAll($sql);

    }

    /**
     * Get words by polish name
     *
     * @access public
     * @param string $polish
     * @param int $idCategories
     * @return array associative array
     */
    public function getWordByName($polish, $idCategories)
    {
        $sql = "SELECT *
        FROM Vocabulary WHERE polish = ? AND idCategories = ?";
         return $this->_db->fetchAssoc(
             $sql, array((string) $polish, $idCategories
            )
         );
    }





    /**
     * Save vocabulary
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function saveVocabulary($data)
    {
        if (isset($data['idVocabulary']) && ctype_digit(
            (string)$data['idVocabulary']
        )) {
            $sql = 'UPDATE Vocabulary SET polish = ?, spanish = ?, comment = ?
            WHERE idVocabulary = ?';
            $this->_db->executeQuery(
                $sql, array( 
                $data['polish'], $data['spanish'], $
                $data['comment'], $data['idVocabulary']
                )
            );
        } else {
            $sql = 'INSERT INTO Vocabulary (polish, 
                spanish, comment) VALUES (?,?,?)';
            $this->_db->executeQuery(
                $sql, array($data['polish'], $data['spanish'], $data['comment'])
            );
        }
    }

    /**
     * Get word by it's ID
     *
     * @access public
     * @param int $idVocabulary
     * @return array associative array
     */
    public function getVocabulary($idVocabulary)
    {
        if (($idVocabulary != '') && ctype_digit((string)$idVocabulary)) {
            $sql = "SELECT *
            FROM Vocabulary WHERE idVocabulary= '$idVocabulary'";
            return $this->_db->fetchAssoc($sql, array((int) $idVocabulary));
        } else {
            return array();
        }
    }

   /**
     * Delete
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function deleteVocabulary($data)
    {
        $sql = 'DELETE FROM Vocabulary WHERE idVocabulary = ?';
        $this->_db->executeQuery($sql, array($data['idVocabulary']));
    }


    /**
     * Check if ID exists
     *
     * @access public
     * @param int $idVocabulary
     * @return array 
     */
    public function idExist($idVocabulary)
    {
        if (($idVocabulary != '') && ctype_digit((string)$idVocabulary)) {
            $sql = 'SELECT idVocabulary, polish, spanish 
            FROM Vocabulary WHERE idVocabulary= ?';
            if ($this->_db->executeUpdate(
                $sql, array((int) $idVocabulary)
            ) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}





