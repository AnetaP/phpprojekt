<?php
/**
 * 
 * @author Aneta Pityńska
 * @copyright Aneta Pityńska, 2014
 * @package Model
 */


/**
 * Define namespace and components.
 * @uses Silex\Application;
 * @uses Silex\Provider\DoctrineServiceProvider;
 */
namespace Model;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

/**
* Define categories database methods.
*/
class categoryModel
{
    /**
    * Database access object.
    *
    * @access protected
    * @var $_db Doctrine\DBAL
    */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app 
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

     /**
     * Add new category
     *
     * @access public
     * @param array $data
     * @param int $typeCategories
     * @param int $idUsers
     * @return Void
     */
    public function addCategory($data, $typeCategories, $idUsers)
    {
        $sql = 'INSERT INTO Categories (nameCategories, typeCategories, idUser) 
        VALUES (?,?,?)';
        $this->_db->executeQuery(
            $sql, array( 
            $data['nameCategories'], $typeCategories , $idUsers)
        );
    }

    /**
     * Get all categories
     *
     * @access public
     * @return array 
     */
    public function getAll()
    {
        $sql = 'SELECT idCategories, nameCategories, typeCategories, idUser 
        FROM Categories';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Get only public categories
     *
     * @access public
     * @return array 
     */
    public function getPublic()
    {
        $sql = 'SELECT idCategories, nameCategories, typeCategories, idUser 
        FROM Categories WHERE idUser= 1 ';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Get categories created by a certain user
     *
     * @access public
     * @param int $idUsers
     * @return array 
     */
    public function getUserCategories($idUsers)
    {
        $sql = "SELECT idCategories, nameCategories, typeCategories, idUser 
        FROM Categories WHERE idUser = (".$idUsers.")";
        return $this->_db->fetchAll($sql);
    }

    /**
     * Save category
     *
     * @access public
     * @param array $data
     * @return Void
     */
    public function saveCategory($data)
    {
        if (isset($data['idCategories']) && ctype_digit(
            (string)$data['idCategories']
        )) {
            $sql = 'UPDATE Categories SET nameCategories = ?, typeCategories = ?
            WHERE idCategories = ?';
            $this->_db->executeQuery(
                $sql, array( 
                $data['nameCategories'], $data['typeCategories'], 
                $data['idCategories'])
            );
        } else {
            $sql = 'INSERT INTO Categories (
                nameCategories, typeCategories) VALUES (?,?)';
            $this->_db->executeQuery(
                $sql, array(
                $data['nameCategories'], $data['typeCategories'])
            );
        }
    }






    /**
     * Get category by ID
     *
     * @access public
     * @param int $idCategories
     * @return array associative array
     */
    public function getCategory($idCategories)
    {
        if (($idCategories != '') && ctype_digit((string)$idCategories)) {
            $sql = 'SELECT idCategories, nameCategories, typeCategories, idUser
            FROM Categories WHERE idCategories= ?';
            return $this->_db->fetchAssoc($sql, array((int) $idCategories));
        } else {
            return array();
        }
    }

    /**
     * Get category by name
     *
     * @access public
     * @param string $nameCategories
     * @param int $idUser users might have categories with the same name
     * @return array associative array
     */
    public function getCategoryByName($nameCategories, $idUser)
    {
      
            $sql = 'SELECT *
            FROM Categories WHERE nameCategories= ? AND idUser = ? ';
            return $this->_db->fetchAssoc(
                $sql, array((string) $nameCategories, $idUser)
            );
 

    }




    /**
     * Delete category
     *
     * @access public
     * @param array $data
     * @return void
     */
    public function deleteCategory($data)
    {
        $sql = 'DELETE FROM Categories WHERE idCategories = ?;
                DELETE FROM Vocabulary WHERE idCategories = ?;';
        $this->_db->executeQuery(
            $sql, array(
            $data['idCategories'], $data['idCategories']
            )
        );
    }

    /**
     * Check if Category exists
     *
     * @access public
     * @param int $idCategories
     * @return boolean
     */
    public function idExist($idCategories)
    {
        if (($idCategories != '') && ctype_digit((string)$idCategories)) {
            $sql = 'SELECT idCategories, nameCategories, typeCategories 
            FROM Categories WHERE idCategories= ?';
            if ($this->_db->executeUpdate(
                $sql, array((int) $idCategories)
            ) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }





     
}





